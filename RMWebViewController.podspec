#
# Be sure to run `pod spec lint RMWebViewController.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# To learn more about the attributes see http://docs.cocoapods.org/specification.html
#
Pod::Spec.new do |s|
  s.name         = "RMWebViewController"
  s.version      = "1.0.0"
  s.summary      = "RMWebViewController specialised SVWebViewController."
  s.homepage     = "http://EXAMPLE/RMWebViewController"
  s.license      = 'MIT'

  s.author       = { "Roland Meijs" => "rmeijs@gmail.com" }
  s.source      =  { :git => '../localpods/RMGMGridView' }
  s.platform     = :ios, '5.0'
  s.source_files = 'RMWebViewController/RMWebViewController.{h,m}'
#  s.dependency 'JSONKit', '~> 1.4'
  s.dependency 'SVWebViewController'
  s.dependency 'MBProgressHUD'
end
