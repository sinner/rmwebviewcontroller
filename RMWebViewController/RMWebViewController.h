//
//  RMWebViewController.h
//  compact
//
//  Created by Roland Meijs on 06-12-12.
//  Copyright (c) 2012 IJenIJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMWebViewController : UIViewController

@property (nonatomic, assign) BOOL refresh;

- (id)initWithAddress:(NSString *)urlString;
- (id)initWithURL:(NSURL *)URL;

@end
