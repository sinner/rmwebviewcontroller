//
//  RMWebViewController.m
//  compact
//
//  Created by Roland Meijs on 06-12-12.
//  Copyright (c) 2012 IJenIJ. All rights reserved.
//

#import "RMWebViewController.h"
#import "SVModalWebViewController.h"
#import "MBProgressHUD.h"

@interface RMWebViewController () <UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *mainWebView;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) MBProgressHUD *HUD;
@end

@implementation RMWebViewController

- (id)initWithAddress:(NSString *)urlString {
    return [self initWithURL:[NSURL URLWithString:urlString]];
}

- (id)initWithURL:(NSURL *)pageURL {
    if (self = [super init]) {
        self.refresh = NO;
        self.URL = pageURL;
    }

    return self;
}

#pragma mark - View lifecycle

- (void)loadView {
    self.mainWebView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mainWebView.delegate = self;
    self.mainWebView.scalesPageToFit = YES;
    [self.mainWebView loadRequest:[NSURLRequest requestWithURL:self.URL ]];
    self.view = self.mainWebView;
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.labelText = @"loading";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.refresh) {
        [self.mainWebView loadRequest:[NSURLRequest requestWithURL:self.URL ]];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.HUD show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.HUD hide:YES];
    self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.HUD hide:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
//        NSLog(@"Let's open: %@", request.URL);
        SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:request.URL];
        webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
        webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
        [self presentViewController:webViewController animated:YES completion:^{
            //
        }];
        return NO;
    } else return YES;
}

@end
