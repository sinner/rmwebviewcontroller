//
//  main.m
//  RMWebView
//
//  Created by Roland Meijs on 07-04-13.
//  Copyright (c) 2013 Roland Meijs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
