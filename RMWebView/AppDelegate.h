//
//  AppDelegate.h
//  RMWebView
//
//  Created by Roland Meijs on 07-04-13.
//  Copyright (c) 2013 Roland Meijs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
